<?php

require_once 'connect_db.php';

session_start();

if (!isset($_SESSION['logged_id_user_id'])) {
    header("location: login.php");
    exit;
}


$logged_id_user_id = $_SESSION['logged_id_user_id'];

$dbConnection = getMysqlConnection();

$sql = "SELECT u.*, r.is_admin, r.is_can_post, r.is_can_edit, r.is_can_delete FROM users u LEFT JOIN role_permissions r ON r.id = u.role_permission_id WHERE u.id = {$logged_id_user_id}";
$result = $dbConnection->query($sql) or die($dbConnection->error);
if ($result->num_rows != 1) {
    header("location: login.php");
    exit;
}

$userLoggedIn = $result->fetch_assoc();


?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta charset="utf-8">
        <title>Blog List</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    </head>
    <body>
        <div class="container">
            <h1 style="text-align: center">Blog List (<a href="logout.php">Log out</a>)</h1>
            <?php
            // ตรวจสอบ  permission ว่าโพสท์ได้ไหม
            if ($userLoggedIn['is_can_post']) {
                ?>
            <form class="" action="?" method="post">
                <div class="row">
                  <div class="col-10">
                      <input class="form-control" type="text" name="message" value="" placeholder="Some thing need to post.">
                  </div>
                  <div class="col-2">
                    <input class="btn btn-primary btn-block" type="submit" name="add" value="Add">
                  </div>
                </div>
            </form>
            <hr>
        <?php
            } ?>

        </div>
    </body>
</html>

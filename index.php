<?php

require_once 'connect_db.php';

session_start();

if (isset($_SESSION['logged_id_user_id'])) {
    $logged_id_user_id = $_SESSION['logged_id_user_id'];

    $dbConnection = getMysqlConnection();

    $sql = "SELECT * FROM users WHERE id = {$logged_id_user_id}";
    $result = $dbConnection->query($sql);
    if ($result->num_rows > 0) {
        header("location: blog.php");
        exit;
    }
    $dbConnection->close();
}

header("location: login.php");

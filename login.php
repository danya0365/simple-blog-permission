<?php

require_once 'connect_db.php';

session_start();

$error = null;

if (isset($_REQUEST['submit'])) {
    $name = $_POST['name'];
    $password = $_POST['password'];

    $dbConnection = getMysqlConnection();

    $sql = "SELECT * FROM users WHERE name = '{$name}' AND password = '{$password}' ";
    $result = $dbConnection->query($sql) or die($dbConnection->error);

    if ($result->num_rows == 1) {
        $userRow = $result->fetch_assoc();
        if ($userRow['name'] == $name && $userRow['password'] == $password) {
            $_SESSION['logged_id_user_id'] = $userRow['id'];
            header("location: blog.php");
            exit;
        }
        $error = "User or password incorrected";
        print_r($userRow);
    } else {
        $error = "User or password incorrected";
    }

    $dbConnection->close();
}

?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta charset="utf-8">
        <title>เข้าสู่ระบบ</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    </head>
    <body>
        <div class="container">
            <h1 style="text-align: center">เข้าสู่ระบบ</h1>
            <?php
            if ($error) {
                echo '<h2>' . $error . '</h2>';
            }
            ?>
            <form class="" action="?" method="post">
                <div class="form-group">
                  <label for="exampleInputEmail1">User</label>
                  <input type="text" class="form-control" name="name" value="test1">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Password</label>
                  <input type="password" class="form-control" name="password" value="test1">
                </div>
                <button type="submit" class="btn btn-primary" name="submit">Submit</button>
            </form>

        </div>
    </body>
</html>
